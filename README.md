# HackScript

## [Example Script](https://bitbucket.org/liquidobsidian/hackscript/src/2c1b4e2d174a7efeafdc867c3a724925a7140a4a/example_script.js?at=master&fileviewer=file-view-default) ##

### Methods
##### *Memory*
###### Returns a handle to the process that goes by that name. Returns false if it fails.
    getProcessByName(name)
###### Returns the base address of the module that goes by that name. Returns false if it fails.
    getModuleAddress(name)
###### Writes an integer to an address.
    writeInt(process, address, value)
###### Writes a float to an address.
    writeFloat(process, address, value)
###### Reads an integer from an address.
    readInt(process, address, value)
###### Reads a float from an address.
    readFloat(process, address, value)
##### *Miscellaneous*
###### Returns the state of the key.
    isKeyDown(virtualKeyCode)
###### Sleeps for however many milliseconds.
    sleep(ms)
###### Sets the print color (0-255)
    printColor(color)