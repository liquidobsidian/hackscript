//==================================================================//
// File:		miscfuncs.h
// Purpose:		Miscellaneous functions of the hack API.
//==================================================================//

int isKeyDown(duk_context *ctx)
{
	int vkKey = duk_require_int(ctx, 0);

	duk_push_boolean(ctx, GetAsyncKeyState(vkKey));

	return 1;
}

int sleep(duk_context *ctx)
{
	Sleep(duk_require_int(ctx, 0));

	return 1;
}

int printColor(duk_context *ctx)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(hConsole, duk_require_int(ctx, 0));

	return 1;
}