//==================================================================//
// File:		memfuncs.h
// Purpose:		Memory-handling functions of the hack API.
//==================================================================//

int getProcessByName(duk_context *ctx)
{
	const char* name = duk_require_string(ctx, 0);

	const size_t cSize = strlen(name) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, name, cSize);

	HANDLE handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 entry;

	entry.dwSize = sizeof(entry);

	do
	if (!wcscmp(entry.szExeFile, wc))
	{
		DWORD procID = entry.th32ProcessID;
		CloseHandle(handle);

		HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, false, procID);

		duk_push_pointer(ctx, proc);

		return 1;
	}
	while (Process32Next(handle, &entry));

	duk_push_boolean(ctx, false);

	return 1;
}

int getModuleAddress(duk_context *ctx)
{
	HANDLE process = duk_require_pointer(ctx, 0);
	const char* name = duk_require_string(ctx, 1);

	DWORD procID = GetProcessId(process);

	const size_t cSize = strlen(name) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, name, cSize);

	HANDLE module = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, procID);
	MODULEENTRY32 entry;
	entry.dwSize = sizeof(entry);

	do
	if (wcscmp(entry.szModule, wc) == 0)
	{
		CloseHandle(module);

		duk_push_uint(ctx, (DWORD)entry.hModule);

		return 1;
	}
	while (Module32Next(module, &entry));

	duk_push_boolean(ctx, false);

	return 1;
}

int writeInt(duk_context *ctx)
{
	HANDLE process = duk_require_pointer(ctx, 0);
	DWORD address = duk_require_uint(ctx, 1);
	int value = duk_require_int(ctx, 2);

	duk_push_boolean(ctx, WriteProcessMemory(process, (LPVOID)address, &value, sizeof(int), 0));

	return 1;
}

int writeFloat(duk_context *ctx)
{
	HANDLE process = duk_require_pointer(ctx, 0);
	DWORD address = duk_require_uint(ctx, 1);
	float value = (float)duk_require_number(ctx, 2);

	duk_push_boolean(ctx, WriteProcessMemory(process, (LPVOID)address, &value, sizeof(float), 0));

	return 1;
}

int readInt(duk_context *ctx)
{
	HANDLE process = duk_require_pointer(ctx, 0);
	DWORD address = duk_require_uint(ctx, 1);

	int value;
	ReadProcessMemory(process, (LPVOID)address, &value, sizeof(int), NULL);

	duk_push_number(ctx, value);

	return 1;
}

int readFloat(duk_context *ctx)
{
	HANDLE process = duk_require_pointer(ctx, 0);
	DWORD address = duk_require_uint(ctx, 1);

	float value;
	ReadProcessMemory(process, (LPVOID)address, &value, sizeof(float), NULL);

	duk_push_number(ctx, value);

	return 1;
}