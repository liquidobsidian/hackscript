//==================================================================//
// File:		main.cpp
// Purpose:		Main source code.
//==================================================================//

#include "main.h"

extern "C" {

	int main(int argc, char *argv[])
	{
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

		SetConsoleTextAttribute(hConsole, 15);
		std::cout << "[";

		SetConsoleTextAttribute(hConsole, 10);
		std::cout << "HackScript v1.0 by Liquid Obsidian.";

		SetConsoleTextAttribute(hConsole, 15);
		std::cout << "]";

		SetConsoleTextAttribute(hConsole, 7);
		std::cout << "\n\n";

		// Get Duk context.
		duk_context *ctx = duk_create_heap_default();

		// Push the context.
		duk_push_global_object(ctx);

		// Add functions
		duk_push_c_function(ctx, getProcessByName, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "getProcessByName");
		duk_push_c_function(ctx, getModuleAddress, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "getModuleAddress");
		duk_push_c_function(ctx, writeInt, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "writeInt");
		duk_push_c_function(ctx, writeFloat, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "writeFloat");
		duk_push_c_function(ctx, readInt, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "readInt");
		duk_push_c_function(ctx, readFloat, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "readFloat");
		duk_push_c_function(ctx, isKeyDown, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "isKeyDown");
		duk_push_c_function(ctx, sleep, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "sleep");
		duk_push_c_function(ctx, printColor, DUK_VARARGS);
		duk_put_prop_string(ctx, -2, "printColor");
		duk_pop(ctx);

		// Run file.
		if (argv[1] != NULL)
		{
			// Read file from argv[1]
			char *file_contents;
			long input_file_size;
			FILE *input_file = fopen(argv[1], "rb");
			fseek(input_file, 0, SEEK_END);
			input_file_size = ftell(input_file);
			rewind(input_file);
			file_contents = (char*)malloc((input_file_size + 1) * (sizeof(char)));
			fread(file_contents, sizeof(char), input_file_size, input_file);
			fclose(input_file);
			file_contents[input_file_size] = 0;

			// Eval the file's contents.
			duk_eval_string(ctx, file_contents);
			duk_pop(ctx);
		}
		else
		{
			// Didn't even bother to provide a file, WHAT A NERD.
			puts("[ERROR] No file specified.\n\nUsage: HackScript <file>\n\nPress enter to exit.");
		}

		// We want the person to actually READ the output.
		getchar();

		return 0;
	}

}