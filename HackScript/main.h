//==================================================================//
// File:		main.h
// Purpose:		Include headers.
//==================================================================//

#include <stdio.h>
#include <windows.h>
#include <TlHelp32.h>
#include <iostream>

#define WIN32_LEAN_AND_MEAN

#include "duktape.h"
#include "memfuncs.h"
#include "miscfuncs.h"