/*
 *  JavaScript CS:GO Hack
 *  	by Liquid Obsidian.
 *  
 *  Includes a working bunnyhop and triggerbot.
 */

printColor(11);
print("JavaScript CS:GO Hack by Liquid Obsidian.\n");

/*
 *  Attach to CS:GO
 */

printColor(14);
print("Finding csgo.exe...");

var csgo;

while (!csgo) {
	csgo = getProcessByName("csgo.exe");
	sleep(1);
}

printColor(10);
print("Found csgo.exe!");

printColor(7); /* reset print color incase they ran it in cmd */

/*
 *  Grab Modules
 */

var dwClient = getModuleAddress(csgo, "client.dll");
var dwEngine = getModuleAddress(csgo, "engine.dll");

/*
 *  Offsets
 */

var dwJump = 0x4AF150C;
var dwAttack = 0x2EC89E8;
var dwLocalPlayer = 0xA6E444;
var dwEntityList = 0x4A5C9C4;
var dwTeam = 0xF0;
var dwFlags = 0x100;
var dwInCross = 0xA940;

/*
 *  Bhop
 */

function bhop() {
	if (!isKeyDown(32)) return; // if not holding space then don't run
	
	var pLocalPlayer = readInt(csgo, dwClient + dwLocalPlayer); // get the local player
	var iFlags = readInt(csgo, pLocalPlayer + dwFlags); // get the local player's flags
	
	if (iFlags == 257) { // if flags == FL_ONGROUND
		writeInt(csgo, dwClient + dwJump, 5); // write 5 to the jump pointer (+jump)
	} else {
		writeInt(csgo, dwClient + dwJump, 4); // write 4 to the jump pointer (-jump)
	}
}

/*
 *  Trigger
 */

function trigger() {
	if (!isKeyDown(18)) return; // if not holding alt
	
	var pLocalPlayer = readInt(csgo, dwClient + dwLocalPlayer); // get the local player
	var iLocalPlayerTeam = readInt(csgo, pLocalPlayer + dwTeam);
	
	var iInCross = readInt(csgo, pLocalPlayer + dwInCross); // gets the index of a potential player in the crosshair
	
	if (!(iInCross > 0 && iInCross < 64)) return; // if the player isn't valid then stop
	
	var pInCross = readInt(csgo, dwClient + dwEntityList + ((iInCross - 1) * 0x10)); // gets the incross player pointer
	var iInCrossTeam = readInt(csgo, pInCross + dwTeam); // gets the team of the incross player
	
	if (iInCrossTeam == iLocalPlayerTeam) return; // if they are a teammate then stop
	
	writeInt(csgo, dwClient + dwAttack, 5); // write 5 to the attack pointer (+attack)
	sleep(1);
	writeInt(csgo, dwClient + dwAttack, 4); // write 4 to the attack pointer (-attack)
}

/*
 *  Run the hack in a loop.
 */

while (true) {
	bhop();
	trigger();
		
	sleep(1);
}